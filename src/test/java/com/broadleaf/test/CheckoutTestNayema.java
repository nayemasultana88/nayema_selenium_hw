/**
 * 
 */
package com.broadleaf.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * @author Nayema
 *
 */
public class CheckoutTestNayema {
     WebDriver driver;
     
     @BeforeMethod
     public void testStart() {
    	 WebDriverManager.chromedriver().setup();
    	 driver = new ChromeDriver();
    	 driver.get("https://demo.broadleafcommerce.org/");
    	 driver.manage().window().fullscreen();
    	 
     }
    @Test
    public void checkOutTest() {
    	Actions mouseAction = new Actions(driver);
    	mouseAction
    	.moveToElement(driver.findElement(By.xpath("//*[@id=\"left-nav\"]/ul/li[3]/a")))
    	.build()
    	.perform();
    	driver.findElement(By.xpath("//*[@id=\"left-nav\"]/ul/li[3]/ul/li[1]/a")).click();
    	driver.findElement(By.xpath("//*[@id=\"products\"]/div[3]/div")).click();
    	driver.findElement(By.xpath("//*[@id=\"product_content\"]/div[2]/div[3]/div[2]/div/a[2]/div")).click();
    	driver.findElement(By.xpath("//*[@id=\"product_content\"]/div[2]/div[3]/div[3]/div/select")).click();
    	driver.findElement(By.xpath("//*[@id=\"product_content\"]/div[2]/div[3]/div[3]/div/select/option[2]")).click();
    	driver.findElements(By.tagName("button")).get(3).click();
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	driver.findElement(By.xpath("//a[@href='/cart']")).click();
    	driver.findElement(By.xpath("//*[@id=\"cart\"]/div[3]/div[1]/div[2]/div[5]/div[1]/a")).click();
    	driver.findElements(By.tagName("button")).get(3).click();
    	driver.findElement(By.id("fullName")).sendKeys("Sayeda Abdullah");
    	driver.findElement(By.id("addressLine1")).sendKeys("Astoria");
    	driver.findElement(By.id("addressLine2")).sendKeys("Queens");
    	driver.findElement(By.id("city")).sendKeys("Long Island");
    	driver.findElement(By.id("stateProvinceRegion")).sendKeys("NY");
    	driver.findElement(By.id("postalCode")).sendKeys("11105");
    	driver.findElement(By.id("phonePrimary")).sendKeys("987654321");
    	driver.findElement(By.xpath("//*[@id=\"shippingInfo\"]/div/div[2]/div[3]/label")).click();
    	driver.findElement(By.xpath("//*[@id=\"checkout\"]/div[2]/div/div[1]/div[2]/div[2]/a")).click();
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	driver.findElement(By.xpath("//*[@id=\"payment_methods\"]/div[1]/div/ul/li[3]/a")).click();
    	driver.findElement(By.xpath("//*[@id=\"checkout\"]/div[2]/div/div[2]/div[2]/div[3]/div[2]/a")).click();
    	driver.findElement(By.xpath("//*[@id=\"checkout\"]/div[2]/div/div[3]/div[2]/div[2]/a")).click();
    }
    
	@AfterMethod
	public void testFinish() {
	  driver.close();
	  driver.quit();
     }
}
    
     
     
     
     

