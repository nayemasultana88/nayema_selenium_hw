package com.broadleaf.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class RegistrationTest {
	WebDriver driver;
	
    @BeforeMethod
	public void testStart() {
    	WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://demo.broadleafcommerce.org/");
		driver.manage().window().fullscreen();
	}
	
	@Test
	public void registrationTest() {
		driver.findElement(By.linkText("LOGIN")).click();
		driver.findElement(By.id("customer.emailAddress")).sendKeys("nayema123@gmail.com");
		driver.findElement(By.id("customer.firstName")).sendKeys("Sayeda");
		driver.findElement(By.id("customer.lastName")).sendKeys("Abdullah");
		driver.findElements(By.id("password")).get(1).sendKeys("12345ABcd");
		driver.findElement(By.id("passwordConfirm")).sendKeys("12345ABcd");
		driver.findElements(By.tagName("button")).get(3).click();
	}
	@AfterMethod
	public void testFinish() {
	  driver.close();
	  driver.quit();
     }
}