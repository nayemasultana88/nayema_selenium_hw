package com.broadleaf.test;

import static org.junit.Assert.fail;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BroadLeafDemoTest {
	  private WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();
	  
	  @BeforeClass(alwaysRun = true)
	  public void setUp() throws Exception {
	    driver = new FirefoxDriver();
	    baseUrl = "https://www.katalon.com/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  }

	  @Test
	  public void testBroadLeaf() throws Exception {
	    driver.get("https://demo.broadleafcommerce.org/");
	    driver.findElement(By.linkText("Login")).click();
	    driver.findElement(By.id("customer.emailAddress")).click();
	    driver.findElement(By.id("customer.emailAddress")).clear();
	    driver.findElement(By.id("customer.emailAddress")).sendKeys("nayema.elahi@gmail.com");
	    driver.findElement(By.id("customer.firstName")).click();
	    driver.findElement(By.id("customer.firstName")).clear();
	    driver.findElement(By.id("customer.firstName")).sendKeys("nayema");
	    driver.findElement(By.id("customer.lastName")).click();
	    driver.findElement(By.id("customer.lastName")).clear();
	    driver.findElement(By.id("customer.lastName")).sendKeys("elahi");
	    driver.findElement(By.xpath("(//input[@id='password'])[2]")).click();
	    driver.findElement(By.xpath("(//input[@id='password'])[2]")).clear();
	    driver.findElement(By.xpath("(//input[@id='password'])[2]")).sendKeys("12345");
	    driver.findElement(By.id("passwordConfirm")).click();
	    driver.findElement(By.id("passwordConfirm")).clear();
	    driver.findElement(By.id("passwordConfirm")).sendKeys("12345");
	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();
	  }

	  @AfterClass(alwaysRun = true)
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}
